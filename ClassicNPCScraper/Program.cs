﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace ClassicNPCScraper
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Classic warcraft NPC scraper\n\nPress Enter to start");
            Console.ReadLine();

            List<NPC> allNpcs = new List<NPC>();

            const string baseAddress = "http://dbe.wowdb.cn/npc.aspx?c=7";
            string newAddress = baseAddress;

            int pages = 1;
            bool nextPage = true;
            

            do
            {
                string html = GetHtml(newAddress);
                allNpcs.AddRange(GetNpcsFromPage(html));

                if ((!Regex.IsMatch(html, "Next")))//try and get the page count for the foum
                {
                    nextPage = false;
                    
                    Console.WriteLine("No more pages");
                }
                pages++;
                newAddress = baseAddress + "&page=" + pages;
                
            } while (nextPage);

            TextWriter tw = new StreamWriter("C:\\Humanoids.txt");
            foreach (NPC npc in allNpcs)
            {
                tw.WriteLine(npc.Name + "(" + npc.Id + ") " + npc.Location);
            }

            XmlSerializer xs = new XmlSerializer(typeof(List<NPC>));
            using (FileStream fs = new FileStream("C:\\Humanoids.xml", FileMode.Create))
            {
                xs.Serialize(fs, allNpcs);
            }

            Console.WriteLine("Finished");
            Console.ReadKey();
        }

        public static List<NPC> GetNpcsFromPage(string sourcecode)
        {
            TextWriter tw = new StreamWriter("C:\\lol.txt");
            
            //Posts.AddRange(from Match m in r.Matches(Program.GetHtml("http://wow.allakhazam.com/forum.html?forum=21&mid=" + Id)) select new Post(m.ToString()));

            //Regex r = new Regex("<tr class=\"itemtr\" onmouseover=\"this.style.backgroundColor='#383838'\" onmouseout=\"this.style.backgroundColor='#181818'\" style=\"color:(?: w|W)hite; ?background-color:.+?;\">(?:.|\\n)+<a href=\"npc-([0-9]+).html\">(.+)</a>(?:.|\n)+<td class=\"itemtd\" align=\"center\" style=\"width:15%;\">\\n\\s+(.+)\n\\s+</td>(?:.|\\n)+</tr>");
            Regex r = new Regex("<tr class=\"itemtr\" onmouseover=\"this.style.backgroundColor='#383838'\" onmouseout=\"this.style.backgroundColor='#181818'\"(?:.|\\n)+?</tr>");
            //Console.WriteLine(r.Match(sourcecode));
            List<NPC> list = new List<NPC>();
            foreach (Match m in r.Matches(sourcecode))
            {
                tw.WriteLine(m + "\n\n");
                tw.Flush();
                Match m2 = Regex.Match(m.ToString(), "<a href=\"npc-([0-9]+).html\">(.+)</a>(?:.|\\n)+?");

                Match m3 = Regex.Match(m.ToString(), @"15%.+>(\s+.+\s+)</td>");

                string s = m3.Groups[1].ToString().Trim();
                Console.WriteLine(s);
                list.Add(new NPC { Id = int.Parse(m2.Groups[1].ToString()), Name = m2.Groups[2].ToString(), Location = s });
            }
            tw.Close();
            return list;
        }

        public static string GetHtml(string url)
        {
            Console.WriteLine("Getting url: " + url);
            using (WebClient client = new WebClient())
            {
                try
                {
                    return client.DownloadString(url);
                }
                catch (WebException) //catch 404
                {
                    return "";
                }
            }
        }
    }

    public class NPC
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public string Location { get; set; }
    }
}
